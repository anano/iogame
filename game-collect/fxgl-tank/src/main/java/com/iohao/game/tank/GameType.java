package com.iohao.game.tank;

/**
 * @author 洛朱
 * @date 2022-03-06
 */
public enum GameType {
    /** 玩家 */
    PLAYER,

    /** 玩家 */
    ENEMY,

    /** 子弹 */
    BULLET,
}
